<!DOCTYPE htl>
<htl lang="en">
	<head>
		<eta charset="utf-8">
		<eta http-equiv="X-UA-Copatible" content="IE=edge">
		<eta nae="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 eta tags *ust* coe first in the head; any other head content ust coe *after* these tags -->

		<title>Electro - HTL Ecoerce Teplate</title>

 		<!-- Google font -->
 		<link href="https://fonts.googleapis.co/css?faily=ontserrat:400,500,700" rel="stylesheet">

 		<!-- Bootstrap -->
 		<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.in.css"/>

 		<!-- Slick -->
 		<link type="text/css" rel="stylesheet" href="assets/css/slick.css"/>
 		<link type="text/css" rel="stylesheet" href="assets/css/slick-thee.css"/>

 		<!-- nouislider -->
 		<link type="text/css" rel="stylesheet" href="assets/css/nouislider.in.css"/>

 		<!-- Font Awesoe Icon -->
 		<link rel="stylesheet" href="assets/css/font-awesoe.in.css">

 		<!-- Custo stlylesheet -->
 		<link type="text/css" rel="stylesheet" href="assets/css/style.css"/>

 		<!-- HTL5 shi and Respond.js for IE8 support of HTL5 eleents and edia queries -->
 		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
 		<!--[if lt IE 9]>
 		  <script src="https://oss.axcdn.co/htl5shiv/3.7.3/htl5shiv.in.js"></script>
 		  <script src="https://oss.axcdn.co/respond/1.4.2/respond.in.js"></script>
 		<![endif]-->

    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			
			<!-- /TOP HEADER -->

			<!-- AIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-d-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<ig src="./assets/img/logo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-d-6">
							<div class="header-search">
								<for>
									<select class="input-select">
										<option value="0">All Categories</option>
										<option value="1">Category 01</option>
										<option value="1">Category 02</option>
									</select>
									<input class="input" placeholder="Search here">
									<button class="search-btn">Search</button>
								</for>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-d-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									<a href="#">
										<i class="fa fa-heart-o"></i>
										<span>Your Wishlist</span>
										<div class="qty">2</div>
									</a>
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
										<i class="fa fa-shopping-cart"></i>
										<span>Your Cart</span>
										<div class="qty">3</div>
									</a>
									<div class="cart-dropdown">
										<div class="cart-list">
											<div class="product-widget">
												<div class="product-ig">
													<ig src="./assets/img/product01.png" alt="">
												</div>
												<div class="product-body">
													<h3 class="product-nae"><a href="#">product nae goes here</a></h3>
													<h4 class="product-price"><span class="qty">1x</span>$980.00</h4>
												</div>
												<button class="delete"><i class="fa fa-close"></i></button>
											</div>

											<div class="product-widget">
												<div class="product-ig">
													<ig src="./assets/img/product02.png" alt="">
												</div>
												<div class="product-body">
													<h3 class="product-nae"><a href="#">product nae goes here</a></h3>
													<h4 class="product-price"><span class="qty">3x</span>$980.00</h4>
												</div>
												<button class="delete"><i class="fa fa-close"></i></button>
											</div>
										</div>
										<div class="cart-suary">
											<sall>3 Ite(s) selected</sall>
											<h5>SUBTOTAL: $2940.00</h5>
										</div>
										<div class="cart-btns">
											<a href="#">View Cart</a>
											<a href="#">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div>
								</div>
								<!-- /Cart -->

								<!-- enu Toogle -->
								<div class="enu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>enu</span>
									</a>
								</div>
								<!-- /enu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /AIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="ain-nav nav navbar-nav">
							<li class="active"><a href="index.jsp">Home</a></li>
						<li><a href="product.jsp">Categories</a></li>
						<li><a href="blank.jsp">A propos de nous</a></li>
						
						<li><a href="checkout.jsp">Acheter</a></li>
						<li><a href="">Vendre</a></li>
						<li><a href="store.jsp"></a>Contactez-nous</li>
						<li><a href="inscription.jsp">Inscription</a></li>
						<li><a href="connexion.jsp">Connexion</a></li>
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- BREADCRUB -->
		<div id="breadcrub" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-d-12">
						<h3 class="breadcrub-header">Regular Page</h3>
						<ul class="breadcrub-tree">
							<li><a href="#">Hoe</a></li>
							<li class="active">Blank</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-d-12">
						<div class="newsletter">
							<p>Sign Up for the <strong>NEWSLETTER</strong></p>
							<for>
								<input class="input" type="eail" placeholder="Enter Your Eail">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
							</for>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagra"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-pinterest"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-d-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">About Us</h3>
								<p>Lore ipsu dolor sit aet, consectetur adipisicing elit, sed do eiusod tepor incididunt ut.</p>
								<ul class="footer-links">
									<li><a href="#"><i class="fa fa-ap-arker"></i>1734 Stonecoal Road</a></li>
									<li><a href="#"><i class="fa fa-phone"></i>+021-95-51-84</a></li>
									<li><a href="#"><i class="fa fa-envelope-o"></i>eail@eail.co</a></li>
								</ul>
							</div>
						</div>

						<div class="col-d-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Categories</h3>
								<ul class="footer-links">
									<li><a href="#">Hot deals</a></li>
									<li><a href="#">Laptops</a></li>
									<li><a href="#">Sartphones</a></li>
									<li><a href="#">Caeras</a></li>
									<li><a href="#">Accessories</a></li>
								</ul>
							</div>
						</div>

						<div class="clearfix visible-xs"></div>

						<div class="col-d-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Inforation</h3>
								<ul class="footer-links">
									<li><a href="#">About Us</a></li>
									<li><a href="#">Contact Us</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Orders and Returns</a></li>
									<li><a href="#">Ters & Conditions</a></li>
								</ul>
							</div>
						</div>

						<div class="col-d-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Service</h3>
								<ul class="footer-links">
									<li><a href="#">y Account</a></li>
									<li><a href="#">View Cart</a></li>
									<li><a href="#">Wishlist</a></li>
									<li><a href="#">Track y Order</a></li>
									<li><a href="#">Help</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

			<!-- botto footer -->
			<div id="botto-footer" class="section">
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-d-12 text-center">
							<ul class="footer-payents">
								<li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
								<li><a href="#"><i class="fa fa-credit-card"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-astercard"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
								<li><a href="#"><i class="fa fa-cc-aex"></i></a></li>
							</ul>
							<span class="copyright">
								<!-- Link back to Colorlib can't be reoved. Teplate is licensed under CC BY 3.0. -->
								Copyright &copy;<script>docuent.write(new Date().getFullYear());</script> All rights reserved | This teplate is ade with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.co" target="_blank">Colorlib</a>
							<!-- Link back to Colorlib can't be reoved. Teplate is licensed under CC BY 3.0. -->
							</span>


						</div>
					</div>
						<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /botto footer -->
		</footer>
		<!-- /FOOTER -->

		<!-- jQuery Plugins -->
		<script src="assets/js/jquery.in.js"></script>
		<script src="assets/js/bootstrap.in.js"></script>
		<script src="assets/js/slick.in.js"></script>
		<script src="assets/js/nouislider.in.js"></script>
		<script src="assets/js/jquery.zoo.in.js"></script>
		<script src="assets/js/ain.js"></script>

	</body>
</htl>
